package com.risk.assessment.controller;


import com.risk.assessment.model.RiskAssessment;
import com.risk.assessment.service.RiskAssessmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/risk")
class RiskAssessmentController {

    @Autowired
    RiskAssessmentService riskAssessmentService;

    @GetMapping("/getRiskAssessment")
    private List<RiskAssessment> getRiskAssessment() {
        return riskAssessmentService.getRiskAssessment();
    };



}
