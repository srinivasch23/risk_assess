package com.risk.assessment.model;

import lombok.Getter;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Table(name ="customer_accounts")
public class CustomerAccounts {
	
	@Id
	@Column(name ="account_key")
	private String accountKey;
	
	@Column(name ="primary_party_key")
	private String primaryPartyKey;
	
	@Column(name ="acct_open_date")
	private Date acctOpenDate;
		
}
