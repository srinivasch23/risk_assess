package com.risk.assessment.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Setter
@Getter
@Entity
@Table(name ="risk_assessment")
public class RiskAssessment {
	@Id
	private String customerId;
	private String riskFactor;
}
