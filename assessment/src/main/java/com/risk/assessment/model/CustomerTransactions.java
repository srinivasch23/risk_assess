package com.risk.assessment.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name ="customer_transactions")
public class CustomerTransactions {
	
	@Id
	@Column(name ="Transfer_key")
	private String transferKey;
	
	@Column(name ="Account_Key")
	private String accountKey;
	
	@Column(name ="Transaction_Amount")
	private double transactionAmount;
	
	@Column(name ="Transaction_Type")
	private String transactionType;
	
	@Column(name ="Transaction_Origin_Destination")
	private String transactionOriginDestination;
	
	@Column(name ="Transaction_Date")
	private Date transactionDate;
		
}
