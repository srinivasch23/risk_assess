package com.risk.assessment.model;

import lombok.Getter;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Table(name ="customers")
public class Customers {
	
	@Id
	@Column(name ="PARTY_KEY")
	private String partyKey;
	
	@Column(name ="residential_country_cd")
	private String residentialCountryCode;
	
	@Column(name ="party_open_date")
	private Date partyOpenDate;
		
}
