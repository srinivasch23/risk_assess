package com.risk.assessment.repository;

import com.risk.assessment.model.CustomerTransactions;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionRepository extends CrudRepository<CustomerTransactions, String> {
    @Query(value= "SELECT  Account_Key" +
            "    FROM customer_transactions " +
            " where Account_Key = :accountKey " +
            "    and Transaction_Origin_Destination IN ( " +
            "     select ENTITY_KEY from countries ) " +
            "    GROUP BY  " +
            "        MONTH(Transaction_Date), Transaction_Type, Account_Key " +
            " having Count(*) > 10", nativeQuery = true)
    List<String> getHighRiskRecords(String accountKey);

    @Query(value= "SELECT Account_Key " +
            "    FROM customer_transactions " +
            " where Account_Key = :accountKey " +
            "    GROUP BY  " +
            "        MONTH(Transaction_Date), Transaction_Type, Account_Key " +
            " having Count(*) > 6", nativeQuery = true)
    List<String> getMediumRiskRecords(String accountKey);

    @Query(value= "SELECT Account_Key" +
            "    FROM customer_transactions " +
            " where Account_Key = :accountKey " +
            "    and Transaction_Origin_Destination NOT IN ( " +
            "     select ENTITY_KEY from countries ) " +
            "    GROUP BY  " +
            "        MONTH(Transaction_Date), Transaction_Type, Account_Key " +
            " having Count(*) < 10", nativeQuery = true)
    List<String> getLowRiskRecords(String accountKey);
    
    
    @Query(value = "SELECT Account_Key " +
            "  FROM  customer_transactions " +
            "  where Account_Key = :accountKey " +
            "        and Transaction_Type = :transactionType " +
            "  GROUP BY  " +
            "   MONTH(Transaction_Date), Account_Key " +
            "  having sum(Transaction_Amount) > :amount", nativeQuery = true)
    List<String> getH2RiskRecords(String accountKey, String transactionType, Double amount);

    @Query(value = "SELECT Account_Key " +
            "  FROM customer_transactions " +
            "  where Account_Key = :accountKey " +
            "        and Transaction_Type = :transactionType " +
            "  GROUP BY  " +
            "   MONTH(Transaction_Date), Account_Key " +
            "  having sum(Transaction_Amount) > :minAmount" +
            " and sum(Transaction_Amount) < :maxAmount", nativeQuery = true)
    List<String> getH2RiskRecords(String accountKey, String transactionType, Double maxAmount, Double minAmount);

    @Query(value = "SELECT Account_Key " +
            "  FROM  customer_transactions " +
            "  where Account_Key = :accountKey " +
            "  GROUP BY " +
            "   Transaction_Date, Account_Key " +
            "  having count(*) > :noOfTrans", nativeQuery = true)
    List<String> getH4RiskRecords(String accountKey, int noOfTrans);

    @Query(value = "SELECT Account_Key " +
            "  FROM  customer_transactions " +
            "  where Account_Key = :accountKey " +
            "  GROUP BY " +
            "   Transaction_Date, Account_Key " +
            "  having count(*) > :minTrans and " +
            " count(*) < :maxTrans", nativeQuery = true)
    List<String> getH4RiskRecords(String accountKey, int maxTrans , int minTrans);
}
