package com.risk.assessment.repository;

import com.risk.assessment.model.Countries;
import com.risk.assessment.model.CustomerAccounts;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountriesRepository extends CrudRepository<Countries, String> {
}
