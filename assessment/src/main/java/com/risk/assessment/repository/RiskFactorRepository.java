package com.risk.assessment.repository;

import com.risk.assessment.model.RiskAssessment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RiskFactorRepository extends CrudRepository<RiskAssessment, String> {
}
