package com.risk.assessment.repository;

import com.risk.assessment.model.CustomerAccounts;
import com.risk.assessment.model.Customers;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomersRepository extends CrudRepository<Customers, String> {
}
