package com.risk.assessment.repository;

import com.risk.assessment.model.CustomerAccounts;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerAccountsRepository extends CrudRepository<CustomerAccounts, String> {
    List<CustomerAccounts> findByPrimaryPartyKey(String primaryPartyKey);
}
