package com.risk.assessment.service;

import com.risk.assessment.model.RiskAssessment;
import org.springframework.stereotype.Service;

import java.util.List;

public interface RiskAssessmentService {
    List<RiskAssessment> getRiskAssessment();
}
