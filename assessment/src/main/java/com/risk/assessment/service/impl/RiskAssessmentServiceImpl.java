package com.risk.assessment.service.impl;

import com.risk.assessment.model.CustomerAccounts;
import com.risk.assessment.model.Customers;
import com.risk.assessment.model.RiskAssessment;
import com.risk.assessment.repository.CustomerAccountsRepository;
import com.risk.assessment.repository.CustomersRepository;
import com.risk.assessment.repository.RiskFactorRepository;
import com.risk.assessment.repository.TransactionRepository;
import com.risk.assessment.service.RiskAssessmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RiskAssessmentServiceImpl implements RiskAssessmentService {

    @Autowired
    private CustomersRepository customersRepository;

    @Autowired
    private CustomerAccountsRepository customerAccountsRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private RiskFactorRepository riskFactorRepository;

    @Override
    public List<RiskAssessment> getRiskAssessment() {
        List<RiskAssessment> riskAssessmentList = new ArrayList<>();
        List <Customers> customersList = (List<Customers>) customersRepository.findAll();
        for (Customers customer: customersList) {
            List<CustomerAccounts> accounts = customerAccountsRepository.findByPrimaryPartyKey(customer.getPartyKey());
            RiskAssessment riskAssessment = new RiskAssessment();
            riskAssessment.setCustomerId(customer.getPartyKey());
            if(!accounts.isEmpty()) {
                riskAssessment.setRiskFactor(getRiskFactor(customer, accounts.get(0)));
            }
            riskAssessmentList.add(riskAssessment);
        }
        riskFactorRepository.deleteAll();
        riskFactorRepository.saveAll(riskAssessmentList);
        return riskAssessmentList;
    }

    public String getRiskFactor(Customers customers, CustomerAccounts accounts) {
        String highRiskFactor = null;
        String mediumRiskFactor = null;
        String lowRiskFactor = null;
        highRiskFactor = getHighRiskFactor(customers, accounts);
        if (highRiskFactor == null) {
            mediumRiskFactor = getMediumRiskFactor(customers, accounts);
            if (mediumRiskFactor == null) {
                lowRiskFactor = getLowRiskFactor(customers, accounts);
                return lowRiskFactor;
            } else
                return mediumRiskFactor;
        }
           return highRiskFactor;

    }

    private String getLowRiskFactor(Customers customers, CustomerAccounts accounts) {
        boolean l1 = false;
        boolean l2 = false;
        boolean l3 = false;
        boolean l4 = false;

        l1=getL1RiskFactor(customers, accounts);
        l2=getL2RiskFactor(customers, accounts);
        l3=getL3RiskFactor(customers, accounts);
        l4=getL4RiskFactor(customers, accounts);

        return l1?"L1":(l2?"L2":(l3?"L3":(l4?"L4":null)));

    }

    private boolean getL4RiskFactor(Customers customers, CustomerAccounts accounts) {
        List<String> riskRecords = transactionRepository.getH4RiskRecords(accounts.getAccountKey(), 10,
                0);
        return !riskRecords.isEmpty();
    }

    private boolean getL3RiskFactor(Customers customers, CustomerAccounts accounts) {
        List<String> riskRecords = transactionRepository.getH2RiskRecords(accounts.getAccountKey(), "OUT",
                500D, 0D);
        return !riskRecords.isEmpty();
    }

    private boolean getL2RiskFactor(Customers customers, CustomerAccounts accounts) {
        List<String> riskRecords = transactionRepository.getH2RiskRecords(accounts.getAccountKey(), "INN",
                600D, 0D);
        return !riskRecords.isEmpty();
    }

    private boolean getL1RiskFactor(Customers customers, CustomerAccounts accounts) {
        List<String> riskRecords = transactionRepository.getLowRiskRecords(accounts.getAccountKey());
        return !riskRecords.isEmpty();
    }

    private String getMediumRiskFactor(Customers customers, CustomerAccounts accounts) {

        boolean m1 = false;
        boolean m2 = false;
        boolean m3 = false;
        boolean m4 = false;

        m1=getM1RiskFactor(customers, accounts);
        m2=getM2RiskFactor(customers, accounts);
        m3=getM3RiskFactor(customers, accounts);
        m4=getM4RiskFactor(customers, accounts);

        return m1?"M1":(m2?"M2":(m3?"M3":(m4?"M4":null)));

    }

    private boolean getM4RiskFactor(Customers customers, CustomerAccounts accounts) {
        List<String> riskRecords = transactionRepository.getH4RiskRecords(accounts.getAccountKey(), 20,
                 10);
        return !riskRecords.isEmpty();
    }

    private boolean getM3RiskFactor(Customers customers, CustomerAccounts accounts) {
        List<String> riskRecords = transactionRepository.getH2RiskRecords(accounts.getAccountKey(), "OUT",
                800D, 500D);
        return !riskRecords.isEmpty();
    }

    private boolean getM2RiskFactor(Customers customers, CustomerAccounts accounts) {
        List<String> riskRecords = transactionRepository.getH2RiskRecords(accounts.getAccountKey(), "INN",
                1000D, 600D);
        return !riskRecords.isEmpty();
    }

    private boolean getM1RiskFactor(Customers customers, CustomerAccounts accounts) {
        List<String> riskRecords = transactionRepository.getMediumRiskRecords(accounts.getAccountKey());
        return !riskRecords.isEmpty();
    }

    private String getHighRiskFactor(Customers customers, CustomerAccounts accounts) {

        boolean h1 = false;
        boolean h2 = false;
        boolean h3 = false;
        boolean h4 = false;

        h1=getH1RiskFactor(customers, accounts);
        h2=getH2RiskFactor(customers, accounts);
        h3=getH3RiskFactor(customers, accounts);
        h4=getH4RiskFactor(customers, accounts);

        return h1?"H1":(h2?"H2":(h3?"H3":(h4?"H4":null)));

    }

    private boolean getH4RiskFactor(Customers customers, CustomerAccounts accounts) {
        List<String> highRiskRecords = transactionRepository.getH4RiskRecords(accounts.getAccountKey(),
                20);
        return !highRiskRecords.isEmpty();
    }

    private boolean getH3RiskFactor(Customers customers, CustomerAccounts accounts) {
        List<String> highRiskRecords = transactionRepository.getH2RiskRecords(accounts.getAccountKey(),
                "OUT" , 800D);
        return !highRiskRecords.isEmpty();
    }

    private boolean getH2RiskFactor(Customers customers, CustomerAccounts accounts) {
        List<String> highRiskRecords = transactionRepository.getH2RiskRecords(accounts.getAccountKey(),
                "INN" , 1000D);
        return !highRiskRecords.isEmpty();
    }

    private boolean getH1RiskFactor(Customers customers, CustomerAccounts accounts) {
        List<String> highRiskRecords = transactionRepository.getHighRiskRecords(accounts.getAccountKey());
         return !highRiskRecords.isEmpty();
    }
}
